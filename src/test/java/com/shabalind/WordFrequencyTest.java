package com.shabalind;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

public class WordFrequencyTest {
    List<String> wordsList = new ArrayList<String>();

    @Before
    public void executedBeforeEach() {
        wordsList = new ArrayList();
        for (int i = 1; i < 1001; i++) {
            if(i%10 == 0) wordsList.add("decade");
            if(i%100 == 0) wordsList.add("hundred");
            if(i%1000 == 0) wordsList.add("thousand");
            wordsList.add("some");
        }
    }

    @Test
    public void getTopFrequentTest(){
        SortedMap<Integer, List<String>> topFrequent = new WordFrequency<String>(wordsList.iterator()).getTopFrequent(10);
        System.out.println(topFrequent.toString());
        Assert.assertTrue("Most frequent is some", "some".equals(topFrequent.get(topFrequent.firstKey()).get(0)));
        Assert.assertTrue("Most not frequent is thousand", "thousand".equals(topFrequent.get(topFrequent.lastKey()).get(0)));
    }

    @Test
    public void getFrequencySortedMapTest(){
        SortedMap<Integer, List<String>> sortedMap = new WordFrequency<String>(wordsList.iterator()).getFrequencySortedMap();
        Assert.assertTrue("sorted map must be [[some], [decade], [hundred], [thousand]]", "[[some], [decade], [hundred], [thousand]]".equals(sortedMap.values().toString()));

    }
}
