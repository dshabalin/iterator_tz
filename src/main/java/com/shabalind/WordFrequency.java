package com.shabalind;


import java.util.*;

/**
 * Class used for
 * create frequency TreeMap and op frequency word by iterator for using words
 * class Map using TreeMap functionality for sort and safe elements
 *
 * @param <T> the type of mapped values
 * @author  dmitry shabalin
 */
public class WordFrequency<T> {

    private Integer size;
    private Iterator<T> iterator;
    Map<T, Integer> frequencyMap;

    private static final Comparator comp = new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o2.compareTo(o1);
        }
    };


    /**
     * Constructs a new WordFrequency class with iterator private field
     * keys.
     *
     * @param size limit size processing elements by iterator by default size is Integer.MAX_VALUE
     * @param iterator
     */
    public WordFrequency(Iterator<T> iterator, int size) {
        this.iterator = iterator;
        if(size <= 0 ) throw new IllegalArgumentException("finite count of elements must be >= 0");
        this.size = size;
        this.frequencyMap = getFrequencyMap();
    }

    public WordFrequency(Iterator<T> iterator) {
        this.iterator = iterator;
        this.size = Integer.MAX_VALUE;
        this.frequencyMap = getFrequencyMap();

    }

    /**
     * Method create Map(String --> Integer), String is word, Integer is frequency
     *
     * @return frequencyMap frequency map of using word by iterator
     */
    private Map<T, Integer> getFrequencyMap(){
        Map<T, Integer> frequencyMap = new HashMap<T, Integer>();
        for (int i = 0; i < size && iterator.hasNext(); i++) {
            T element = iterator.next();
            if(frequencyMap.containsKey(element)){
                frequencyMap.put(element, frequencyMap.get(element) + 1);
            }else {
                frequencyMap.put(element, 1);
            }
        }
        return frequencyMap;
    }


    /**
     * Method sort frequency map (String --> Integer)
     * sorting is resolving by using TreeMap
     *
     * @return sorted map (Integer --> List<String>) where Integer is the number of occurrences, List<String> - words
     */
    public TreeMap<Integer, List<T>> getFrequencySortedMap(){
        TreeMap<Integer, List<T>> sortedMap = new TreeMap<Integer, List<T>>(comp);

        //get all keys from frequencyMap by value and add to sortedMap(value --> {'key1', 'key2'...})
        for(final Map.Entry<T, Integer> entry: frequencyMap.entrySet()){
            if(sortedMap.containsKey(entry.getValue())){
                sortedMap.get(entry.getValue()).add(entry.getKey());
            }else{
                List<T> values = new ArrayList<T>(1);
                values.add(entry.getKey());
                sortedMap.put(entry.getValue(), values);
            }
        }
        return sortedMap;
    }

    /**
     * Method return topCount entries from frequent entries from getFrequencySortedMap
     * sorting is resolving by using TreeMap
     *
     * @param topCount limits size returned map if topCount < 1 or topCount  > counts of entries in frequencySortedMap
     *                 then method return frequencySortedMap fully.
     * @return sorted map (Integer --> List<String>) where Integer is the number of occurrences, List<String> - words
     */
    public SortedMap<Integer, List<T>> getTopFrequent(int topCount){
        TreeMap<Integer, List<T>> frequencySortedMap = getFrequencySortedMap();
        if(topCount >= frequencySortedMap.size() || topCount < 1){
            return  frequencySortedMap;
        }else {

            TreeMap<Integer, List<T>>  frequencyTopSortedMap = new TreeMap<Integer, List<T>>(comp);
            Iterator<Integer> iter = frequencySortedMap.keySet().iterator();
            for (int i = 0; i < topCount && iter.hasNext(); i++) {
                Integer key = iter.next();
                frequencyTopSortedMap.put(key, frequencySortedMap.get(key));
            }
            return frequencyTopSortedMap;
        }

    }

    @Override
    public String toString(){
        return frequencyMap.toString();
    }
}





































