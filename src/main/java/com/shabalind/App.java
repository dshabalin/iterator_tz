package com.shabalind;

import java.util.*;

/**
 *
 */
public class App {
    public static void main(String[] args) {
        Random random = new Random();
        List<String> wordsList = new ArrayList<String>();
        for (int i = 0; i < 1000; i++) {
            wordsList.add("some" + String.valueOf(random.nextInt(20)));
        }

        WordFrequency<String> frequency = new WordFrequency<String>(wordsList.iterator(), 400);
        System.out.println("******************************************************");
        System.out.println("all:");
        System.out.println(frequency.toString());

        System.out.println("******************************************************");
        System.out.println("only top:");
        System.out.println(frequency.getTopFrequent(2).toString());

    }
}
